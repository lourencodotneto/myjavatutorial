package com.example.demo;

import com.example.restservice.Greeting;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@SpringBootApplication
@RestController
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        /* "@RequestParam" binds the value of the query string parameter "name" into the "name" parameter of
        the "greeting()" method. If the "name" parameter is absent in the request, the "defaultValue" of
        "World" is used
         */

         /*
        The implementation of the method body creates and returns a new "Greeting" object with "id" and
        "content" attributes based on the next value from he "counter" and formats the given "name" by using the greeting "template"
         */

		return new Greeting(counter.incrementAndGet(), String.format(template, name));


	}

}
